<?php
namespace Gungnir\Session\Bag;

class AttributeBag implements Bag
{
    private $name = 'attribute_bag';

    private $data = [];

    /**
     * Gets the name of this bag
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Sets the name of this bag
     *
     * @return self
     */
    public function setName(string $name) : Bag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Store an item in this bag under the given name
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return self
     */
    public function store(string $name, $value) : Bag
    {
        $this->data[$name] = $value;
        return $this;
    }

    /**
     * Checks if an item by the passed name exists in this
     * bag
     *
     * @param  string  $name
     *
     * @return boolean
     */
    public function has(string $name) : bool
    {
        return isset($this->data[$name]);
    }

    /**
     * Retrieves an item in the bag by the given name
     *
     * @param  string $name
     * @return mixed
     */
    public function get(string $name)
    {
        return $this->data[$name] ?? false;
    }

    /**
     * Removes item in this bag by the given name
     *
     * @param  string $name
     *
     * @return bool
     */
    public function remove(string $name) : bool
    {
        if ($this->has($name)) {
            unset($this->data[$name]);
        }
        return $this->has($name);
    }

    /**
     * Empties the bag of all items
     *
     * @return self
     */
    public function clear() : Bag
    {
        $this->data = [];
        return $this;
    }

    public function fill(array $data) : Bag
    {
        $this->data = $data;
        return $this;
    }

    public function pour() : array
    {
        return $this->data;
    }
}
