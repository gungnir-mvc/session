<?php
namespace Gungnir\Session\Bag;

interface Bag
{
    /**
     * Gets the name of this bag
     *
     * @return string
     */
    public function getName() : string;

    /**
     * Sets the name of this bag
     *
     * @return self
     */
    public function setName(string $bagName) : self;

    /**
     * Store an item in this bag under the given name
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return self
     */
    public function store(string $name, $value) : self;

    /**
     * Retrieves an item in the bag by the given name
     *
     * @param  string $name
     * @return mixed
     */
    public function get(string $name);

    /**
     * Checks if an item by the passed name exists in this
     * bag
     *
     * @param  string  $name
     *
     * @return boolean
     */
    public function has(string $name) : bool;

    /**
     * Removes item in this bag by the given name
     *
     * @param  string $name
     *
     * @return bool
     */
    public function remove(string $name) : bool;

    /**
     * Empties the bag of all items
     *
     * @return self
     */
    public function clear() : self;

    public function fill(array $data) : self;

    public function pour() : array;
}
