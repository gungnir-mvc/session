<?php
namespace Gungnir\Session;

use \Gungnir\Session\Exception\Bag\MissingBagException;
use \Gungnir\Session\Bag\{AttributeBag, Bag};

class NativeSessionStorage implements SessionStorage
{
    /** @var int The session id */
    private $id = false;

    /** @var boolean */
    private $started = false;

    /** @var boolean */
    private $closed  = true;

    /** @var Bag[] */
    private $bags = [];

    /**
     * Constructor for NativeSessionStorage
     *
     * @param AttributeBag $attributeBag
     */
    public function __construct(AttributeBag $attributeBag)
    {
        $this->setBag($attributeBag);
    }

    /**
     * Retrieve a registered bag by name
     *
     * @param  String $bagName
     *
     * @throws MissingBagException
     * @return Bag
     */
    public function getBag(string $bagName)
    {
        if (empty($this->bags[$bagName])) {
            throw new MissingBagException('Bag with name ' . $bagName . ' is not present in the session');
        }

        return $this->bags[$bagName];
    }

    /**
     * Binds a bag to the storage
     *
     * @param Bag $bag
     *
     * @return self
     */
    public function setBag(Bag $bag) : SessionStorage
    {
        $this->bags[$bag->getName()] = $bag;
        return $this;
    }

    /**
     * Checks if the session have been started
     *
     * @return boolean
     */
    public function isStarted()
    {
        return $this->started;
    }

    /**
     * Saves everything in bags to the session under given bag name
     * and then closes the session. Which makes it impossible to make
     * any further changes to it.
     *
     * @return self
     */
    public function save() : SessionStorage
    {
        foreach ($this->bags as $bagName => $bag) {
            $_SESSION[$bagName] = $bag->pour();
        }

        session_write_close();
        $this->started = false;
        $this->closed = true;

        return $this;
    }

    /**
     * Loads the session into the session object
     *
     * @return self
     */
    public function loadSession(array $session = null)
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
            session_regenerate_id();
        } elseif (session_status() === PHP_SESSION_DISABLED) {
            throw new \Gungnir\Session\Exception\SessionStorageConfigurationException('Native sessions are disabled, to use native storage for sessions please enable this in PHP configuration.');
        }

        if (null === $session) {
            $session = $_SESSION;
        }

        foreach ($this->bags as $bagName => $bag) {
            if (isset($session[$bagName])) {
                $this->bags[$bagName]->fill($session[$bagName]);
            }
        }

        $this->started = true;
        $this->closed = false;

        return $this;
    }

}
