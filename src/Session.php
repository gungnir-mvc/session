<?php
namespace Gungnir\Session;

use \Gungnir\Session\Bag\Bag;

class Session
{
    private $sessionStorage = null;

    /**
     * Constructor for Session
     *
     * @param SessionStorage $sessionStorage
     */
    public function __construct(SessionStorage $sessionStorage)
    {
        $this->sessionStorage = $sessionStorage;
        $this->sessionStorage->loadSession();
    }

    /**
     * Checks if the session is started
     *
     * @return boolean
     */
    public function isStarted() : bool
    {
        return $this->sessionStorage->isStarted();
    }

    /**
     * Saves and closes the session
     *
     * @return self
     */
    public function save() : Session
    {
        $this->sessionStorage->save();

        return $this;
    }

    /**
     * Starts session handling and reads in any session data
     *
     * @return self
     */
    public function start() : Session
    {
        if (!$this->isStarted()) {
            $this->sessionStorage->loadSession();
        }
        return $this;
    }

    /**
     * Get the main data bag from the session
     *
     * @return Bag
     */
    public function attributes() : Bag
    {
        return $this->sessionStorage->getBag('attribute_bag');
    }

    /**
     * Stores a value bind to the name inside the session
     *
     * @param  string  $name  Name of entry to store in session
     * @param  mixed   $value The value to store in entry
     *
     * @return Session        [description]
     */
    public function store(string $name, $value) : Session
    {
        $this->attributes()->store($name, $value);
        return $this;
    }

    public function get(string $name)
    {
        return $this->attributes()->get($name);
    }

    public function remove(string $name)
    {
        return $this->attributes()->remove($name);
    }

    public function __destruct()
    {
        $this->save();
    }
}
