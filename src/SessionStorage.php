<?php
namespace Gungnir\Session;

use \Gungnir\Session\Bag\bag;

interface SessionStorage
{
    /**
     * Retrieve a registered bag by name
     *
     * @param  String $bagName
     * @return Bag
     */
    public function getBag(string $bagName);

    /**
     * Loads the session into the session object
     *
     * @return self
     */
    public function loadSession(array $session = null);

}
